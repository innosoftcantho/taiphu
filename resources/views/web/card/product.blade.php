<div class="card card-product bg-light shadow border-0 rounded-0 w-100">
    <div class="card-product-main h-100">
        <div class="image d-flex align-items-center justify-content-center w-100 p-3">
            <img class="card-img-top" src="{{ asset($product->avatar)}}">
        </div>
        <div class="card-body text-center text-body pt-0">
            <h5 class="card-title font-weight-bold">{{ $product->product_name }}</h5>
            <div class="font-weight-medium text-danger">{{ number_format($product->bill_price) }} đ</div>
        </div>
        <div class="bg-overlay-2">
            <div class="p-3 text-white">
                {{ ($product->note)}}
            </div>
        </div>
    </div>
    <div class="row d-flex mt-auto">
        <div class="col-12 col-md-6 mb-3 mb-md-0 pr-3 pr-md-0">
            <a href="{{ $product->type() ? url($product->type()->menuType->menu->alias, [$product->alias]) : '#'}}">
                <div class="btn btn-primary btn-block font-weight-medium rounded-0 px-0">Chi tiết</div>
            </a>
        </div>
        <div class="col-12 col-md-6 pl-md-0">
            <div class="btn btn-danger btn-block btn-cart font-weight-medium rounded-0 px-0 ml-auto" onclick="add_cart({{ $product->id }})" style="cursor:pointer">
                <i class="fas fa-shopping-cart text-white"></i> 
            </div>
        </div>
    </div>
</div>
{{-- <div class="card card-product rounded-0 h-100">
    <div class="image d-flex align-items-center justify-content-center w-100 p-2">
        <img class="card-img-top" src="{{ asset($product->avatar)}}">
    </div>
    <div class="card-body text-center text-body">
        <h5 class="card-title font-weight-bold">{{ $product->product_name }}</h5>
        <div class="font-weight-medium">{{ number_format($product->bill_price) }} đ</div>
    </div>
    <div class="row d-flex p-2">
        <div class="col-12 col-md-5 mb-3 mb-md-0 pr-3 pr-md-0">
            <a href="{{ $type->menu() ? url($type->menuType->menu->alias, [$product->alias]) : '#'}}">
                <div class="btn btn-outline-secondary btn-block font-weight-medium px-0">Chi tiết</div>
            </a>
        </div>
        <div class="col-12 col-md-7">
            <div class="btn btn-primary btn-block btn-cart font-weight-medium px-0 ml-auto" onclick="add_cart({{ $product->id }})" style="cursor:pointer">Thêm giỏ hàng</div>
        </div>
    </div>
</div> --}}