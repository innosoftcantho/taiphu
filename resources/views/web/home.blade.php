@extends('layouts.web')
@section('content')

<!-- Slideshow -->
@include('web.home.slideshow')

<!-- Sponsors -->
@include('web.home.sponsors')

<!-- Benefit -->
@include('web.home.benefit')

<!-- Types -->
@include('web.home.types')

<!-- Products -->
@include('web.home.products')

<!-- Featured -->
@include('web.home.featured')

<!-- News -->
@include('web.home.news')

<!-- Succeed -->
{{-- @include('web.home.succeed') --}}


@endsection

@push('js')
    <script>
        // if (screen.width >= 992) {
        //     new WOW().init();
        // }
    </script>
@endpush
