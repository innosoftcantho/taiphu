<div class="container">
    <h2 class="d-md-none text-center text-uppercase font-weight-bold mb-4 mt-3 pb-4 pt-3">
        Chúng tôi cung cấp
    </h2>
    <div class="types bg-light my-5">
        <div class="row">
            @if (count($types) > 0)
                <div class="col-md-4 d-none d-md-inline-block">
                    <div class="d-flex justify-content-md-center align-items-md-center text-center px-2 h-100 w-100">
                        <h4 class="text-uppercase font-weight-bold text-dark">
                            Chúng tôi cung cấp
                        </h4>
                    </div>
                </div>
            @foreach ($types as $type)
                @if ($loop->first)
                <div class="col-md-8">
                    <a href="{{ $type->menu() ? url( $type->menu()->alias) : "#" }}" class="types-overlay d-flex justify-content-md-center align-items-md-center w-100">
                        <img src="{{ asset($type->avatar)}}" class="w-100">
                        <div class="bg-overlay h-100 w-100">
                            <h2 class="bottom-left text-white">{{ $type->type_name }}</h2>
                        </div>
                    </a>
                </div>
                @else
                @endif
            @endforeach
            @endif
        </div>
        <div class="row">
            @foreach ($types as $type)
            @if (!$loop->first)
                <div class="col-md-4 mt-4">
                    <a href="{{ $type->menu() ? url( $type->menu()->alias) : "#" }}" class="types-overlay d-flex justify-content-md-center align-items-md-center h-100 w-100">
                        <img src="{{ asset($type->avatar)}}" class="h-100 w-100">
                        <div class="bg-overlay h-100 w-100">
                            <h2 class="bottom-left text-white">{{ $type->type_name }}</h2>
                        </div>
                    </a>
                </div>
            @else
            @endif
            @endforeach
        </div>
    </div>
</div>