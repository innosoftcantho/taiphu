<div class="home-news my-5">
    <div class="container">
        <h2 class="text-center text-uppercase font-weight-bold mb-4 mt-5 pb-4 pt-5">
            tin tức
        </h2>
        <div class="row">
            {{-- {{ dd($contents) }} --}}
            @foreach ($contents as $content)
                @if ($loop->first)
                <div class="col-lg-6 mb-3">
                    <a href="{{ url("$menu_category->alias/$content->alias") }}">
                        <div class="card news border-0 h-100">
                            <div class="img-vert d-flex align-items-center justify-content-center">
                                <img src="{{ asset($content->avatar)}}" class="img-news card-img-top rounded-0 h-100">
                            </div>
                            <div class="card-body vert p-0 mt-1">
                                <div class="card-title text-justify font-weight-bold mb-0">
                                    {{ $content->title }}
                                </div> 
                                <div class="text-secondary text-justify">
                                    <div class="my-1">
                                        <small class="font-italic">{{ date('d-m-Y', strtotime($content->created_at)) }}</small>
                                    </div>
                                    <div class="card-text font-weight-normal">
                                        {{ $content->summary }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                @else
                @endif
            @endforeach
            <div class="col-lg-6">
                <div class="row">
                    @forelse ($contents ?? [] as $content)
                        @if (!$loop->first)
                        <div class="col-12">
                            <a href="{{ url("$menu_category->alias/$content->alias") }}">
                                <div class="card border-0 mb-3" style="width:100%;">
                                    <div class="news d-flex mb-2">
                                        <div class="col-4 px-0">
                                            <div class="d-flex align-items-start justify-content-center h-100">
                                                <img src="{{ asset($content->avatar)}}" class="img-news w-100">
                                            </div>
                                        </div>
                                        <div class="col-8 pr-0">
                                            <div class="card-body p-0">
                                                <div class="card-title text-justify font-weight-bold mb-0">
                                                    {{ $content->title }}
                                                </div>
                                                <div class="text-secondary text-justify">
                                                    <small class="font-italic">{{ date('d-m-Y', strtotime($content->created_at)) }}</small>
                                                    <div class="card-text font-weight-normal">
                                                        {{ $content->summary }}
                                                    </div>
                                                </div>
                                            </div>   
                                        </div> 
                                    </div>
                                </div>
                            </a>
                        </div>
                        @endif
                    @empty
                        
                    @endforelse
                </div>
            </div>
        </div>
        {{-- <div class="row justify-content-center">
            @forelse ($contents ?? [] as $content)
            <div class="col-12 col-md-8 col-xl-4 mb-4">
                <a href="{{ url("$menu_category->alias/$content->alias") }}">
                    <div class="card rounded-0 h-100 w-100">
                        <img class="w-100" src="{{ asset($content->avatar) }}">
                        <div class="card-body text-body mt-2">
                            <h5 class="card-title text-justify font-weight-bold mb-0">
                                {{ $content->title }}
                            </h5>
                            <div class="time my-3">
                                {{ \Carbon\Carbon::parse($content->created_at)->diffForHumans() }}
                            </div>
                            <div class="card-text text-justify">
                                {{ $content->summary }}
                            </div>
                            <div class="d-flex mt-5">
                                <div class="font-weight-medium fs-14 ml-auto">
                                    Xem tiếp <i class="fas fa-long-arrow-alt-right ml-2"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>    
            </div>
            @empty
            @endforelse
        </div> --}}
    </div>
</div>