/*
 Navicat Premium Data Transfer

 Source Server         : database
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : taiphu

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 15/11/2019 00:12:17
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carousels
-- ----------------------------
DROP TABLE IF EXISTS `carousels`;
CREATE TABLE `carousels`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort` int(11) NULL DEFAULT 0,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `text_overlay` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `carousel_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of carousels
-- ----------------------------
INSERT INTO `carousels` VALUES (1, 0, 'upload/carousels/1.jpeg', '', 'tww', 'tw', '2019-11-14 14:39:47', '2019-11-14 14:39:48');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `category_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `is_recruit` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 0, NULL, 'Danh Muc 1', 1, 1, NULL, 'vi', NULL, '2019-09-25 04:00:16', '2019-09-25 04:00:16', NULL, 0);
INSERT INTO `categories` VALUES (2, 2, 0, NULL, 'Giới Thiệu', 1, 0, 'Trang Giới Thiệu', 'vi', 'Trang Giới Thiệu', '2019-09-25 04:27:25', '2019-09-25 04:27:25', NULL, 0);
INSERT INTO `categories` VALUES (3, 3, 0, NULL, 'Sản Phẩm', 1, 0, 'Trang Sản Phẩm', 'vi', 'Trang Sản Phẩm', '2019-09-25 07:09:19', '2019-09-25 07:09:19', NULL, 0);
INSERT INTO `categories` VALUES (5, 5, 0, NULL, 'Tin Tức', 1, 0, 'Trang Tin Tức', 'vi', 'Trang Tin Tức', '2019-09-25 07:10:06', '2019-09-25 07:10:06', NULL, 0);
INSERT INTO `categories` VALUES (7, 7, 0, NULL, 'Liên Hệ', 1, 0, 'Trang Liên Hệ', 'vi', 'Trang Liên Hệ', '2019-09-25 07:10:44', '2019-09-25 07:10:44', NULL, 0);
INSERT INTO `categories` VALUES (8, 8, 0, NULL, 'Carousel', 1, 0, NULL, 'vi', NULL, '2019-09-25 07:33:08', '2019-10-18 12:34:19', NULL, 0);
INSERT INTO `categories` VALUES (9, 9, 0, NULL, 'Kĩ Thuật', 1, 0, NULL, 'vi', NULL, '2019-09-30 15:45:40', '2019-09-30 15:45:51', '2019-09-30 15:45:51', 0);
INSERT INTO `categories` VALUES (10, 9, 0, 'upload/categories/10.jpeg', 'Banner', 1, 0, 'Home Banner', 'vi', 'Home Banner', '2019-10-08 09:23:10', '2019-10-19 03:22:43', NULL, 0);
INSERT INTO `categories` VALUES (11, 11, 0, NULL, 'sdgfsg', 1, 0, 'gsfgsdfg', 'vi', NULL, '2019-10-15 07:38:40', '2019-10-15 07:38:44', '2019-10-15 07:38:44', 0);

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for content_categories
-- ----------------------------
DROP TABLE IF EXISTS `content_categories`;
CREATE TABLE `content_categories`  (
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`content_id`, `category_id`) USING BTREE,
  INDEX `content_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `content_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `content_categories_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of content_categories
-- ----------------------------
INSERT INTO `content_categories` VALUES (1, 2, 0, 1, 0, 0, '2019-11-13 04:39:27', '2019-11-13 04:39:27', NULL);

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_draft` tinyint(1) NOT NULL DEFAULT 0,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `embed` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `contents_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `contents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contents
-- ----------------------------
INSERT INTO `contents` VALUES (1, 1, 0, NULL, 'Giới Thiệu Công Ty', 'gioi-thieu-cong-ty', 'Trân An Phú là tập đoàn thuỷ sản số 1 Việt Nam và hàng đầu trên thế giới.\r\nSản phẩm của chúng tôi hiện đang có mặt tại hơn 50 quốc gia và vùng lãnh thổ, với doanh thu trên 10,000 tỷ VNĐ mỗi năm.\r\nThông qua việc sở hữu các chuỗi giá trị khép kín và có trách nhiệm.\r\nTrân An Phú đặt mục tiêu xây dựng một hệ sinh thái hoàn chỉnh, mang lại những giá trị tốt đẹp cho tất cả các thành viên liên quan, đưa Việt Nam lên bản đồ thế giới với vị thế là nhà cung ứng tôm chất lượng hàng đầu.', '<p>Tại Tr&acirc;n An Phú, ch&uacute;ng t&ocirc;i kh&ocirc;ng ngừng kết hợp kinh nghiệm, sự s&aacute;ng tạo, v&agrave; tr&aacute;ch nhiệm trong to&agrave;n bộ chuỗi gi&aacute; trị sản xuất t&ocirc;m, từ kh&acirc;u đầu đến kh&acirc;u cuối. Sứ mệnh của ch&uacute;ng t&ocirc;i l&agrave; cung cấp cho thị trường to&agrave;n cầu những sản phẩm t&ocirc;m Việt Nam tốt nhất, sạch nhất, v&agrave; dinh dưỡng nhất; đồng thời mang đến cho người ti&ecirc;u d&ugrave;ng sự an t&acirc;m v&agrave; trải nghiệm tuyệt vời nhất tr&ecirc;n từng b&agrave;n ăn, trong từng bữa ăn.</p>\r\n\r\n<p>Điều tạo n&ecirc;n những gi&aacute; trị kh&aacute;c biệt ở Minh Ph&uacute; đ&oacute; ch&iacute;nh l&agrave; việc ch&uacute;ng t&ocirc;i sản xuất c&aacute;c sản phẩm của m&igrave;nh kh&ocirc;ng chỉ dựa tr&ecirc;n nhu cầu ti&ecirc;u d&ugrave;ng th&ocirc;ng thường, m&agrave; c&ograve;n được th&uacute;c đẩy bởi c&aacute;c gi&aacute; trị lịch sử, văn ho&aacute;, v&agrave; c&aacute;c mục ti&ecirc;u ph&aacute;t triển bền vững như: đảm bảo vệ sinh an to&agrave;n thực phẩm, bảo vệ m&ocirc;i trường, c&acirc;n bằng lợi &iacute;ch x&atilde; hội, v&agrave; quan t&acirc;m đến quyền lợi vật nu&ocirc;i.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, 'Chuyên cung cấp các thiết bị năng lượng mặt trời.', '2019-09-25 04:28:38', '2019-11-13 04:39:27', NULL, 'https://www.youtube.com/watch?v=uJRv0utwSm4', 'uJRv0utwSm4');
INSERT INTO `contents` VALUES (2, 2, 0, 'upload/contents/2.jpeg', 'Carousel-1', 'carousel-1', 'Carousel-1Carousel-1Carousel-1', '<p>Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-25 07:44:17', '2019-10-19 10:25:46', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (16, 16, 1, 'upload/contents/16.jpeg', 'banner', 'banner', 'banner banner banner', '<p>banner banner banner</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-10-08 09:28:59', '2019-10-10 07:24:40', '2019-10-10 07:24:40', NULL, NULL);

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_categories
-- ----------------------------
DROP TABLE IF EXISTS `menu_categories`;
CREATE TABLE `menu_categories`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `is_banner` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`menu_id`, `category_id`) USING BTREE,
  INDEX `menu_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `menu_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_categories_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_categories
-- ----------------------------
INSERT INTO `menu_categories` VALUES (1, 5, '2019-10-09 08:54:46', '2019-10-09 08:54:46', NULL, 0);
INSERT INTO `menu_categories` VALUES (9, 5, '2019-09-25 07:28:10', '2019-09-25 07:28:10', NULL, 0);

-- ----------------------------
-- Table structure for menu_contents
-- ----------------------------
DROP TABLE IF EXISTS `menu_contents`;
CREATE TABLE `menu_contents`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `content_id`) USING BTREE,
  INDEX `menu_contents_content_id_foreign`(`content_id`) USING BTREE,
  CONSTRAINT `menu_contents_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_contents_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_contents
-- ----------------------------
INSERT INTO `menu_contents` VALUES (2, 1, '2019-09-25 07:11:10', '2019-09-25 07:11:10', NULL);

-- ----------------------------
-- Table structure for menu_products
-- ----------------------------
DROP TABLE IF EXISTS `menu_products`;
CREATE TABLE `menu_products`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `product_id`) USING BTREE,
  INDEX `menu_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `menu_products_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_types
-- ----------------------------
DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE `menu_types`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `type_id`) USING BTREE,
  INDEX `menu_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `menu_types_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_types
-- ----------------------------
INSERT INTO `menu_types` VALUES (3, 5, '2019-11-14 14:27:30', '2019-11-14 14:27:30', NULL);
INSERT INTO `menu_types` VALUES (5, 3, '2019-11-14 14:27:45', '2019-11-14 14:27:45', NULL);
INSERT INTO `menu_types` VALUES (6, 2, '2019-11-14 14:27:58', '2019-11-14 14:27:58', NULL);
INSERT INTO `menu_types` VALUES (7, 1, '2019-11-14 14:28:09', '2019-11-14 14:28:09', NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `banner` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 1, 0, 'Trang Chủ', '/', 'home', '', 1, 1, 'vi', NULL, '2019-09-25 03:46:51', '2019-09-25 04:09:27', NULL);
INSERT INTO `menus` VALUES (2, 2, 0, 'Giới Thiệu', 'gioi-thieu', 'about', '', 1, 2, 'vi', NULL, '2019-09-25 04:26:43', '2019-09-25 07:11:10', NULL);
INSERT INTO `menus` VALUES (3, 3, 4, 'Máy Nước Nóng', 'may-nuoc-nong', 'type', '', 1, 1, 'vi', NULL, '2019-09-25 07:08:34', '2019-11-14 14:27:17', NULL);
INSERT INTO `menus` VALUES (4, 4, 0, 'Sản Phẩm', 'san-pham', 'null', '', 1, 3, 'vi', NULL, '2019-09-25 07:16:58', '2019-09-25 07:22:20', NULL);
INSERT INTO `menus` VALUES (5, 5, 4, 'Bồn Inox', 'bon-inox', 'type', '', 1, 2, 'vi', NULL, '2019-09-25 07:24:33', '2019-11-14 14:27:45', NULL);
INSERT INTO `menus` VALUES (6, 6, 4, 'Bồn Nhựa', 'bon-nhua', 'type', '', 1, 3, 'vi', NULL, '2019-09-25 07:25:23', '2019-11-14 14:27:57', NULL);
INSERT INTO `menus` VALUES (7, 7, 4, 'Bồn Thép', 'bon-thep', 'type', '', 1, 4, 'vi', NULL, '2019-09-25 07:25:53', '2019-11-14 14:28:09', NULL);
INSERT INTO `menus` VALUES (9, 9, 0, 'Tin Tức', 'tin-tuc', 'category', NULL, 1, 5, 'vi', NULL, '2019-09-25 07:28:08', '2019-09-25 07:28:08', NULL);
INSERT INTO `menus` VALUES (11, 11, 0, 'Liên Hệ', 'lien-he', 'contact', '', 1, 7, 'vi', NULL, '2019-09-25 07:29:39', '2019-10-08 10:01:59', NULL);
INSERT INTO `menus` VALUES (12, 12, 0, 'Giỏ Hàng', 'gio-hang', 'cart', '', 0, 0, 'vi', NULL, '2019-10-04 07:42:27', '2019-10-08 10:01:01', NULL);
INSERT INTO `menus` VALUES (16, 13, 0, 'Thanh Toán', 'thanh-toan', 'order', '', 0, 0, 'vi', NULL, '2019-10-09 02:24:05', '2019-10-09 02:24:19', NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_06_07_092547_create_types_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_06_07_092557_create_products_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_06_07_092743_create_product_types_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_07_15_171057_create_languages_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_07_15_172233_create_menus_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_07_15_173740_create_contents_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_07_16_101957_create_categories_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_07_16_102230_create_menu_categories_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_07_16_102328_create_menu_contents_table', 1);
INSERT INTO `migrations` VALUES (12, '2019_07_16_102429_create_content_categories_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_07_16_151845_allow_null_target_menus_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_07_19_161724_create_menu_products_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_07_19_161841_create_menu_types_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_08_14_104127_create_sponsors_table', 2);
INSERT INTO `migrations` VALUES (17, '2019_08_21_143215_add_url_target_sort_is_show_to_sponsors_table', 3);
INSERT INTO `migrations` VALUES (19, '2019_09_25_085431_create_carousels_table', 4);
INSERT INTO `migrations` VALUES (21, '2019_09_26_071649_create_contacts_table', 5);
INSERT INTO `migrations` VALUES (23, '2019_09_26_082049_add_video_to_contents_table', 6);
INSERT INTO `migrations` VALUES (27, '2019_09_26_092326_add_is_recruit_to_categories_table', 7);
INSERT INTO `migrations` VALUES (28, '2019_07_23_103255_add_is_featured_to_products_table', 8);
INSERT INTO `migrations` VALUES (30, '2019_10_04_094036_create_sessions_table', 9);
INSERT INTO `migrations` VALUES (35, '2019_10_07_000000_add_alias_to_products_table', 10);
INSERT INTO `migrations` VALUES (36, '2019_10_07_033910_add_description_to_products_table', 10);
INSERT INTO `migrations` VALUES (37, '2019_10_08_094906_add_banner_to_menus_table', 11);
INSERT INTO `migrations` VALUES (38, '2019_10_09_020143_create_payments_table', 12);
INSERT INTO `migrations` VALUES (42, '2019_10_09_082400_add_banner_to_menu_categories_table', 13);
INSERT INTO `migrations` VALUES (43, '2019_10_11_031638_create_orders_table', 14);
INSERT INTO `migrations` VALUES (44, '2019_10_11_032519_create_order_products_table', 15);
INSERT INTO `migrations` VALUES (46, '2019_10_12_083128_add_foreign_key_to_order_products_table', 16);
INSERT INTO `migrations` VALUES (49, '2019_10_12_094225_add_total_to_orders_table', 17);
INSERT INTO `migrations` VALUES (50, '2019_10_29_070521_add_bill_price_to_order_products_table', 18);

-- ----------------------------
-- Table structure for order_products
-- ----------------------------
DROP TABLE IF EXISTS `order_products`;
CREATE TABLE `order_products`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `bill_price` int(10) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`product_id`, `order_id`) USING BTREE,
  INDEX `order_products_order_id_foreign`(`order_id`) USING BTREE,
  CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Họ',
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Điện thoại',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Email',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `sum` int(10) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `payments_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product_types
-- ----------------------------
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE `product_types`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`, `type_id`) USING BTREE,
  INDEX `product_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `product_types_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `product_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Alias',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Mô tả',
  `receipt_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá nhập',
  `bill_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá bán',
  `stock` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Tồn kho',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `is_featured` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Sản phẩm nổi bật',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE INDEX `sessions_id_unique`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sponsors
-- ----------------------------
DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE `sponsors`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `is_show` tinyint(1) NULL DEFAULT 1,
  `sort` int(10) UNSIGNED NULL DEFAULT 0,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `sponsor_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sponsors
-- ----------------------------
INSERT INTO `sponsors` VALUES (4, 'upload/sponsors/4.png', 1, 0, NULL, 'www.solartek.com.vn/', 'Solar Tek', NULL, '2019-09-25 08:42:28', '2019-11-14 14:52:36');
INSERT INTO `sponsors` VALUES (5, 'upload/sponsors/5.png', 1, 0, NULL, 'https://www.megasun.com.vn/v2/', 'MEGASUN', NULL, '2019-09-25 08:43:27', '2019-11-14 14:53:08');
INSERT INTO `sponsors` VALUES (6, 'upload/sponsors/6.png', 1, 0, NULL, '/', 'Solar Energy', NULL, '2019-11-14 14:55:34', '2019-11-14 14:55:34');
INSERT INTO `sponsors` VALUES (7, 'upload/sponsors/7.png', 1, 0, NULL, 'http://www.redsun.com.vn/', 'Mặt Trời Đỏ', NULL, '2019-11-14 14:56:23', '2019-11-14 14:56:24');

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `type_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT 'Sắp xếp',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES (1, NULL, 'Bồn Thép', 1, 1, NULL, '2019-09-25 07:15:36', '2019-11-14 14:26:42', NULL);
INSERT INTO `types` VALUES (2, NULL, 'Bồn Nhựa', 1, 2, NULL, '2019-09-25 07:23:08', '2019-11-14 14:26:35', NULL);
INSERT INTO `types` VALUES (3, NULL, 'Bồn Inox', 1, 3, NULL, '2019-09-25 07:23:27', '2019-11-14 14:26:27', NULL);
INSERT INTO `types` VALUES (4, NULL, 'Diệt Khuẩn', 1, 4, NULL, '2019-09-25 07:23:38', '2019-09-25 07:23:44', '2019-09-25 07:23:44');
INSERT INTO `types` VALUES (5, NULL, 'Máy Nước Nóng', 1, 4, NULL, '2019-09-25 07:23:38', '2019-11-14 14:26:20', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'InnoSoft', 'innosoft', 'innosoftcantho@gmail.com', NULL, '$2y$10$HTPZows7CAP2t76GLm9B5ONH.0ARunk8Zom9kY59qmtTE0rjDYK8G', 1, '28UeSv44RPW7kjfIcVfDTHUZLrJHpAjfLdPW0EQ6N739bMlIayxjMGaB4Ttk', '2019-09-25 03:45:55', '2019-11-13 04:21:59', NULL);

SET FOREIGN_KEY_CHECKS = 1;
