/*
 Navicat Premium Data Transfer

 Source Server         : database
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : taiphu

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 16/11/2019 20:51:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carousels
-- ----------------------------
DROP TABLE IF EXISTS `carousels`;
CREATE TABLE `carousels`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort` int(11) NULL DEFAULT 0,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `text_overlay` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `carousel_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of carousels
-- ----------------------------
INSERT INTO `carousels` VALUES (1, 0, 'upload/carousels/1.jpeg', '', 'tww', 'tw', '2019-11-14 14:39:47', '2019-11-14 14:39:48');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `category_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `is_recruit` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 0, NULL, 'Danh Muc 1', 1, 1, NULL, 'vi', NULL, '2019-09-25 04:00:16', '2019-09-25 04:00:16', NULL, 0);
INSERT INTO `categories` VALUES (2, 2, 0, NULL, 'Giới Thiệu', 1, 0, 'Trang Giới Thiệu', 'vi', 'Trang Giới Thiệu', '2019-09-25 04:27:25', '2019-09-25 04:27:25', NULL, 0);
INSERT INTO `categories` VALUES (3, 3, 0, NULL, 'Sản Phẩm', 1, 0, 'Trang Sản Phẩm', 'vi', 'Trang Sản Phẩm', '2019-09-25 07:09:19', '2019-09-25 07:09:19', NULL, 0);
INSERT INTO `categories` VALUES (5, 5, 0, NULL, 'Tin Tức', 1, 0, 'Trang Tin Tức', 'vi', 'Trang Tin Tức', '2019-09-25 07:10:06', '2019-09-25 07:10:06', NULL, 0);
INSERT INTO `categories` VALUES (7, 7, 0, NULL, 'Liên Hệ', 1, 0, 'Trang Liên Hệ', 'vi', 'Trang Liên Hệ', '2019-09-25 07:10:44', '2019-09-25 07:10:44', NULL, 0);
INSERT INTO `categories` VALUES (8, 8, 0, NULL, 'Carousel', 1, 0, NULL, 'vi', NULL, '2019-09-25 07:33:08', '2019-10-18 12:34:19', NULL, 0);
INSERT INTO `categories` VALUES (9, 9, 0, NULL, 'Kĩ Thuật', 1, 0, NULL, 'vi', NULL, '2019-09-30 15:45:40', '2019-09-30 15:45:51', '2019-09-30 15:45:51', 0);
INSERT INTO `categories` VALUES (10, 9, 0, 'upload/categories/10.jpeg', 'Banner', 1, 0, 'Home Banner', 'vi', 'Home Banner', '2019-10-08 09:23:10', '2019-10-19 03:22:43', NULL, 0);
INSERT INTO `categories` VALUES (11, 11, 0, NULL, 'sdgfsg', 1, 0, 'gsfgsdfg', 'vi', NULL, '2019-10-15 07:38:40', '2019-10-15 07:38:44', '2019-10-15 07:38:44', 0);

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (1, 'Thanh Tai', 'httai96@gmail.com', 'sadfdàd', '2019-11-16 08:14:51', '2019-11-16 08:14:51');

-- ----------------------------
-- Table structure for content_categories
-- ----------------------------
DROP TABLE IF EXISTS `content_categories`;
CREATE TABLE `content_categories`  (
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`content_id`, `category_id`) USING BTREE,
  INDEX `content_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `content_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `content_categories_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of content_categories
-- ----------------------------
INSERT INTO `content_categories` VALUES (1, 2, 0, 1, 0, 0, '2019-11-13 04:39:27', '2019-11-13 04:39:27', NULL);
INSERT INTO `content_categories` VALUES (17, 5, 0, 1, 0, 0, '2019-11-15 17:49:58', '2019-11-15 17:49:58', NULL);
INSERT INTO `content_categories` VALUES (18, 5, 0, 1, 0, 0, '2019-11-16 04:17:50', '2019-11-16 04:17:50', NULL);
INSERT INTO `content_categories` VALUES (19, 5, 0, 1, 0, 0, '2019-11-16 04:20:48', '2019-11-16 04:20:48', NULL);
INSERT INTO `content_categories` VALUES (20, 5, 0, 1, 0, 0, '2019-11-16 04:23:24', '2019-11-16 04:23:24', NULL);

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_draft` tinyint(1) NOT NULL DEFAULT 0,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `embed` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `contents_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `contents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contents
-- ----------------------------
INSERT INTO `contents` VALUES (1, 1, 0, NULL, 'Giới Thiệu Công Ty', 'gioi-thieu-cong-ty', 'Trân An Phú là tập đoàn thuỷ sản số 1 Việt Nam và hàng đầu trên thế giới.\r\nSản phẩm của chúng tôi hiện đang có mặt tại hơn 50 quốc gia và vùng lãnh thổ, với doanh thu trên 10,000 tỷ VNĐ mỗi năm.\r\nThông qua việc sở hữu các chuỗi giá trị khép kín và có trách nhiệm.\r\nTrân An Phú đặt mục tiêu xây dựng một hệ sinh thái hoàn chỉnh, mang lại những giá trị tốt đẹp cho tất cả các thành viên liên quan, đưa Việt Nam lên bản đồ thế giới với vị thế là nhà cung ứng tôm chất lượng hàng đầu.', '<p>Tại Tr&acirc;n An Phú, ch&uacute;ng t&ocirc;i kh&ocirc;ng ngừng kết hợp kinh nghiệm, sự s&aacute;ng tạo, v&agrave; tr&aacute;ch nhiệm trong to&agrave;n bộ chuỗi gi&aacute; trị sản xuất t&ocirc;m, từ kh&acirc;u đầu đến kh&acirc;u cuối. Sứ mệnh của ch&uacute;ng t&ocirc;i l&agrave; cung cấp cho thị trường to&agrave;n cầu những sản phẩm t&ocirc;m Việt Nam tốt nhất, sạch nhất, v&agrave; dinh dưỡng nhất; đồng thời mang đến cho người ti&ecirc;u d&ugrave;ng sự an t&acirc;m v&agrave; trải nghiệm tuyệt vời nhất tr&ecirc;n từng b&agrave;n ăn, trong từng bữa ăn.</p>\r\n\r\n<p>Điều tạo n&ecirc;n những gi&aacute; trị kh&aacute;c biệt ở Minh Ph&uacute; đ&oacute; ch&iacute;nh l&agrave; việc ch&uacute;ng t&ocirc;i sản xuất c&aacute;c sản phẩm của m&igrave;nh kh&ocirc;ng chỉ dựa tr&ecirc;n nhu cầu ti&ecirc;u d&ugrave;ng th&ocirc;ng thường, m&agrave; c&ograve;n được th&uacute;c đẩy bởi c&aacute;c gi&aacute; trị lịch sử, văn ho&aacute;, v&agrave; c&aacute;c mục ti&ecirc;u ph&aacute;t triển bền vững như: đảm bảo vệ sinh an to&agrave;n thực phẩm, bảo vệ m&ocirc;i trường, c&acirc;n bằng lợi &iacute;ch x&atilde; hội, v&agrave; quan t&acirc;m đến quyền lợi vật nu&ocirc;i.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, 'Chuyên cung cấp các thiết bị năng lượng mặt trời.', '2019-09-25 04:28:38', '2019-11-13 04:39:27', NULL, 'https://www.youtube.com/watch?v=uJRv0utwSm4', 'uJRv0utwSm4');
INSERT INTO `contents` VALUES (2, 2, 0, 'upload/contents/2.jpeg', 'Carousel-1', 'carousel-1', 'Carousel-1Carousel-1Carousel-1', '<p>Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-25 07:44:17', '2019-10-19 10:25:46', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (16, 16, 1, 'upload/contents/16.jpeg', 'banner', 'banner', 'banner banner banner', '<p>banner banner banner</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-10-08 09:28:59', '2019-10-10 07:24:40', '2019-10-10 07:24:40', NULL, NULL);
INSERT INTO `contents` VALUES (17, 3, 1, 'upload/contents/17.jpeg', 'CÁCH LẮP ĐẶT BỒN NƯỚC AN TOÀN HIỆU QUẢ', 'cach-lap-dat-bon-nuoc-an-toan-hieu-qua', 'Ngày nay, để tích trữ nước sạch, hầu hết các hộ gia đình đều sử dụng sản phẩm bồn chứa nước bằng Inox hoặc bồn nhựa thay thế cho bể chứa nước bằng gạch xây nặng nề trước đây. Với những tiện ích: dễ dàng lắp đặt, có thể di chuyển, hình thức đẹp, gọn gàng, vệ sinh, giá thành hợp lý, bồn nước trở thành sự lựa chọn ngày càng phổ biến của người tiêu dùng cả ở thành thị và nông thôn.', '<p>Ng&agrave;y nay, để t&iacute;ch trữ nước sạch, hầu hết c&aacute;c hộ gia đ&igrave;nh đều sử dụng sản phẩm bồn chứa nước bằng Inox hoặc bồn nhựa thay thế cho bể chứa nước bằng gạch x&acirc;y nặng nề trước đ&acirc;y. Với những tiện &iacute;ch: dễ d&agrave;ng lắp đặt, c&oacute; thể di chuyển, h&igrave;nh thức đẹp, gọn g&agrave;ng, vệ sinh, gi&aacute; th&agrave;nh hợp l&yacute;, bồn nước trở th&agrave;nh sự lựa chọn ng&agrave;y c&agrave;ng phổ biến của người ti&ecirc;u d&ugrave;ng cả ở th&agrave;nh thị v&agrave; n&ocirc;ng th&ocirc;n.</p>\r\n\r\n<p>Tuy nhi&ecirc;n, trong thời gian gần đ&acirc;y c&oacute; nhiều luồng th&ocirc;ng tin phản &aacute;nh về việc người ti&ecirc;u d&ugrave;ng tự &yacute; lắp đặt bồn nước kh&ocirc;ng đ&uacute;ng với y&ecirc;u cầu kỹ thuật của nh&agrave; sản xuất dẫn đến những nguy hiểm c&oacute; thể xảy ra đối với con người. Vậy, lắp đặt bồn nước như thế n&agrave;o mới đảm bảo được độ an to&agrave;n v&agrave; mang lại hiệu quả sử dụng cao nhất?</p>\r\n\r\n<p><img src=\"https://file.hstatic.net/1000195956/file/img-20141107-00638.jpg\" /></p>\r\n\r\n<p>Trước ti&ecirc;n, người ti&ecirc;u d&ugrave;ng phải t&iacute;nh to&aacute;n cụ thể khả năng chịu lực v&agrave; kết cấu c&ocirc;ng tr&igrave;nh để lựa chọn sản phẩm bồn nước v&agrave; dung t&iacute;ch ph&ugrave; hợp với nhu cầu sử dụng.</p>\r\n\r\n<p>&nbsp;Đọc kĩ c&aacute;c t&agrave;i liệu hướng dẫn sử dụng v&agrave; tuyệt đối tu&acirc;n thủ c&aacute;c lưu &yacute; lắp đặt m&agrave; nh&agrave; sản xuất đ&atilde; ban h&agrave;nh. Khi tiến h&agrave;nh lắp đặt bồn nước, người ti&ecirc;u d&ugrave;ng phải thực hiện theo đ&uacute;ng c&aacute;c quy định sau:</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; Mặt bằng lắp đặt y&ecirc;u cầu phải phẳng v&agrave; c&oacute; độ cứng nhất định, chịu được tải lớn gấp 2 lần tải bồn nước. Vị tr&iacute; lắp đặt kh&ocirc;ng được gần m&eacute;p lan can, ph&iacute;a tr&ecirc;n lối đi lại hay cửa ra v&agrave;o. Khi r&aacute;p th&acirc;n bồn v&agrave;o ch&acirc;n bồn y&ecirc;u cầu bồn phải được đặt tr&ecirc;n ch&acirc;n th&agrave;nh một khối thống nhất, c&acirc;n đối, kh&ocirc;ng được nghi&ecirc;ng về bất cứ ph&iacute;a n&agrave;o. Ba nh&aacute;nh ch&acirc;n chịu lực của sản phẩm phải được k&ecirc; tr&ecirc;n mặt phẳng đồng nhất.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; Khi lắp đặt cần đặc biệt ch&uacute; &yacute; siết chặt c&aacute;c đinh ốc cố định bồn với ch&acirc;n bồn, đường xả nước kh&ocirc;ng chạm tới ch&acirc;n nền; tuyệt đối kh&ocirc;ng thiết kế gi&aacute; đỡ ri&ecirc;ng, kh&ocirc;ng k&ecirc; k&iacute;ch, ch&egrave;n l&oacute;t.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; Đối với c&aacute;c th&aacute;p nước cao n&ecirc;n c&oacute; d&acirc;y đai n&iacute;u c&aacute;c ph&iacute;a tr&aacute;nh gi&oacute; b&atilde;o. Nếu c&oacute; điều kiện c&oacute; thể x&acirc;y tường hoặc bờ bao xung quanh (kh&ocirc;ng cần qu&aacute; cao, chỉ cần cao khoảng 1/3 chiều cao của th&acirc;n bồn).</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; Đậy k&iacute;n nắp bồn tr&aacute;nh c&aacute;c vật bẩn rơi v&agrave;o, đối với bồn Inox chỉ chứa đựng nước đạt ti&ecirc;u chuẩn nước sạch, kh&ocirc;ng chứa nước bị nhiễm ph&egrave;n, nhiễm mặn, nước giếng khoan hoặc nước chưa qua xử l&yacute;.</p>\r\n\r\n<p>-&nbsp;&nbsp;&nbsp; Định kỳ 1 năm 1 lần kiểm tra vị tr&iacute; lắp đặt, ch&acirc;n đế, vệ sinh bồn nước.</p>\r\n\r\n<p><img src=\"https://file.hstatic.net/1000195956/file/quy_trinh_lap_dat_bon_inox.jpg\" /></p>\r\n\r\n<p>Để đảm bảo được độ an to&agrave;n trong qu&aacute; tr&igrave;nh sử dụng, mỗi người h&atilde;y l&agrave; người ti&ecirc;u d&ugrave;ng th&ocirc;ng th&aacute;i, chọn mua v&agrave; sử dụng sản phẩm bồn nước từ c&aacute;c thương hiệu nổi tiếng, c&oacute; uy t&iacute;n tr&ecirc;n thị trường. Đ&oacute; cũng l&agrave; một c&aacute;ch để tiết kiệm được chi ph&iacute; v&agrave; tận dụng tối đa hiệu quả của sản phẩm đồng thời c&ograve;n đảm bảo sức khỏe cho bản th&acirc;n v&agrave; gia đ&igrave;nh.&nbsp;</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-11-15 17:49:57', '2019-11-15 17:49:58', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (18, 18, 1, 'upload/contents/18.jpeg', 'Không biết mẹo, gia chủ lắp bình nước nóng mặt trời rét run', 'khong-biet-meo-gia-chu-lap-binh-nuoc-nong-mat-troi-ret-run', 'Vào mùa đông, dù có nắng nhẹ, nước trong bình năng lượng mặt trời nhà ông Tuấn (Hà Nội) âm ấm, dịp mưa dầm, nước vẫn lạnh nguyên.', '<p>V&agrave;o m&ugrave;a đ&ocirc;ng, d&ugrave; c&oacute; nắng nhẹ, nước trong b&igrave;nh năng lượng mặt trời nh&agrave; &ocirc;ng Tuấn (H&agrave; Nội) &acirc;m ấm, dịp mưa dầm, nước vẫn lạnh nguy&ecirc;n.</p>\r\n\r\n<p>Bốn năm trước, l&uacute;c x&acirc;y nh&agrave; hai tầng, &ocirc;ng Tuấn (Ph&uacute;c Thọ, H&agrave; Nội) quyết định lắp b&igrave;nh năng lượng mặt trời, sau khi được người quen tư vấn nhiều lợi &iacute;ch: Đầu tư một lần d&ugrave;ng được l&acirc;u năm, h&agrave;ng th&aacute;ng kh&ocirc;ng tốn tiền điện, kh&ocirc;ng sợ điện giật, ch&aacute;y nổ như d&ugrave;ng b&igrave;nh nước n&oacute;ng bằng điện hay gas. Con g&aacute;i &ocirc;ng mở tiệm gội đầu trong nh&agrave;, phải thường xuy&ecirc;n d&ugrave;ng nước n&oacute;ng. V&igrave; thế, &ocirc;ng chi hơn 10 triệu lắp b&igrave;nh năng lượng mặt trời 160 l&iacute;t.&nbsp;</p>\r\n\r\n<p>Lắp v&agrave;o dịp h&egrave;, gia đ&igrave;nh &ocirc;ng rất vui v&igrave; ng&agrave;y n&agrave;o nước n&oacute;ng cũng tr&agrave;n trề. Tuy nhi&ecirc;n, sang tới m&ugrave;a đ&ocirc;ng, những ng&agrave;y c&oacute; ch&uacute;t nắng th&igrave; nước chỉ &acirc;m ấm. Dịp n&agrave;o mưa dầm, nước vẫn lạnh nguy&ecirc;n. Gia đ&igrave;nh buộc phải lắp th&ecirc;m hai b&igrave;nh nước n&oacute;ng chạy điện nữa.</p>\r\n\r\n<p>&quot;Cuối c&ugrave;ng, chiếc b&igrave;nh năng lượng mặt trời coi như chẳng cần thiết v&igrave; khi trời lạnh, cần nước n&oacute;ng th&igrave; b&igrave;nh kh&ocirc;ng ph&aacute;t huy t&aacute;c dụng. L&uacute;c nắng to m&ugrave;a h&egrave;, gia đ&igrave;nh rất &iacute;t nhu cầu sử dụng&quot;, &ocirc;ng Tuấn kết luận.</p>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"Vào thời gian mùa đông, nếu lắp thêm một điện trở, gia chủ vẫn có thể sử dụng nước nóng từ bình năng lượng mặt trời. Ảnh: solarquotes.\" src=\"https://i-giadinh.vnecdn.net/2018/11/30/binh2-9431-1543539486.jpg\" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>V&agrave;o thời gian m&ugrave;a đ&ocirc;ng, nếu lắp th&ecirc;m một điện trở, gia chủ vẫn c&oacute; thể sử dụng nước n&oacute;ng từ b&igrave;nh năng lượng mặt trời.&nbsp;Ảnh:&nbsp;<em>solarquotes.</em></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p>Rất nhiều gia đ&igrave;nh ở miền Bắc d&ugrave; đ&atilde; lắp b&igrave;nh năng lượng mặt trời vẫn phải lắp th&ecirc;m b&igrave;nh n&oacute;ng lạnh. Tuy nhi&ecirc;n, theo tiến sĩ Nguyễn Mạnh Cường, giảng vi&ecirc;n Đại học quốc gia TP HCM, đ&oacute; l&agrave; c&aacute;ch l&agrave;m phức tạp h&oacute;a vấn đề v&agrave; kh&ocirc;ng tiết kiệm.</p>\r\n\r\n<p>&quot;Thay v&agrave;o đ&oacute;,&nbsp;<strong>bạn chỉ cần lắp th&ecirc;m một thiết bị gia nhiệt phụ trợ (l&agrave; một điện trở)&nbsp;</strong>trực tiếp với m&aacute;y nước n&oacute;ng năng lượng mặt trời&quot;, tiến sĩ Cường khuy&ecirc;n.</p>\r\n\r\n<p>&quot;Khi trời &acirc;m u, m&aacute;y kh&ocirc;ng thể l&agrave;m n&oacute;ng nước, điện trở sẽ gi&uacute;p n&oacute; trở th&agrave;nh một b&igrave;nh nước n&oacute;ng gi&aacute;n tiếp - như một b&igrave;nh nước n&oacute;ng chạy điện b&igrave;nh thường. Lưu &yacute;, bạn chỉ cấp điện cho điện trở khi bức xạ mặt trời kh&ocirc;ng đủ để gia nhiệt m&agrave; bạn vẫn muốn d&ugrave;ng nước n&oacute;ng. Nếu sử dụng thiết bị n&agrave;y, v&agrave;o m&ugrave;a đ&ocirc;ng, bạn vẫn c&oacute; thể tiết kiệm được 70% tiền điện so với d&ugrave;ng b&igrave;nh n&oacute;ng lạnh chạy điện&quot;, tiến sĩ Cường bổ sung.</p>\r\n\r\n<p>Vị chuy&ecirc;n gia cũng cho biết, việc lắp đặt điện trở kh&aacute; đơn giản.&nbsp;Thiết bị n&agrave;y c&oacute; ở c&aacute;c cửa h&agrave;ng cung cấp m&aacute;y nước n&oacute;ng năng lượng mặt trời cũng như cửa h&agrave;ng điện, với gi&aacute; v&agrave;i trăm ngh&igrave;n đồng (đ&atilde; bao gồm thiết bị bảo vệ chống r&ograve; rỉ điện).&nbsp;</p>\r\n\r\n<table align=\"center\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">\r\n	<tbody>\r\n		<tr>\r\n			<td><img alt=\"Một loại điện trở dùng cho bình năng lượng mặt trời. Ảnh: Vattu.\" src=\"https://i-giadinh.vnecdn.net/2018/11/29/1-vat-tu-5447-1543488894.jpg\" /></td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<p>Một loại điện trở d&ugrave;ng cho b&igrave;nh năng lượng mặt trời. Ảnh:&nbsp;<em>Vattu.</em></p>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><strong>Gia đ&igrave;nh &ocirc;ng To&agrave;n ngụ ở th&agrave;nh phố Th&aacute;i B&igrave;nh</strong>&nbsp;gồm 6 người, c&aacute;ch đ&acirc;y một năm đ&atilde; đầu tư hơn 10 triệu cho một hệ thống m&aacute;y l&agrave;m nước n&oacute;ng năng lượng mặt trời, lắp tr&ecirc;n m&aacute;i nh&agrave;, rồi đưa nước đến nhiều vị tr&iacute; cần sử dụng, như tắm giặt, rửa b&aacute;t, nấu ăn... V&igrave; đ&atilde; lắp thiết bị gia nhiệt phụ trợ n&ecirc;n m&ugrave;a đ&ocirc;ng nh&agrave; &ocirc;ng vẫn c&oacute; nước n&oacute;ng d&ugrave;ng.</p>\r\n\r\n<p>&Ocirc;ng Toản so s&aacute;nh với thời c&ograve;n d&ugrave;ng b&igrave;nh n&oacute;ng lạnh chạy điện, v&agrave;o m&ugrave;a h&egrave;, mỗi th&aacute;ng gia đ&igrave;nh &ocirc;ng tiết kiệm được khoảng 40 số điện, tầm 80.000 đồng v&agrave; 30.000 đồng tiền gas đun nấu. M&ugrave;a đ&ocirc;ng, cũng tiết kiệm được 30 số điện/th&aacute;ng.</p>\r\n\r\n<p>Tiến sĩ Cường nhận x&eacute;t,&nbsp;<strong>do nguy&ecirc;n l&yacute; hoạt động của m&aacute;y năng lượng mặt trời phụ thuộc v&agrave;o thời gian chiếu s&aacute;ng cũng như bức xạ của mặt trời</strong>&nbsp;n&ecirc;n lắp đặt ở miền Nam (kh&ocirc;ng c&oacute; m&ugrave;a đ&ocirc;ng, lượng mưa &iacute;t v&agrave; ngắn, hệ số bức xạ mặt trời cao) hiệu quả hơn ở miền Bắc<strong>.</strong>&nbsp;Ở miền Nam, người sử dụng kh&ocirc;ng cần lắp thiết bị gia nhiệt phụ trợ.</p>\r\n\r\n<p>M&aacute;y ph&ugrave; hợp cho hầu hết c&aacute;c hộ gia đ&igrave;nh, kh&aacute;ch sạn, nh&agrave; h&agrave;ng, resort, bệnh viện.... Tr&ecirc;n thị trường c&oacute; nhiều d&ograve;ng m&aacute;y ph&ugrave; hợp với quy m&ocirc; hộ gia đ&igrave;nh c&oacute; tuổi thọ 12-15 năm.</p>\r\n\r\n<p>M&aacute;y c&oacute; thể l&agrave;m nước n&oacute;ng trong thời gian ngắn, nhiệt độ l&agrave;m n&oacute;ng trung b&igrave;nh l&agrave; 55 - 65 độ C (thậm ch&iacute; l&ecirc;n đến 100 độ C t&ugrave;y thời tiết). B&igrave;nh bảo &ocirc;n c&oacute; thể giữ nhiệt được v&agrave;i ng&agrave;y, thất tho&aacute;t nhiệt trung b&igrave;nh khoảng 4 độ C/ng&agrave;y.</p>\r\n\r\n<p><strong>Để sử dụng m&aacute;y nước n&oacute;ng năng lượng mặt trời hiệu quả</strong>,&nbsp;tiến sĩ Cường khuyến c&aacute;o người ti&ecirc;u d&ugrave;ng n&ecirc;n đặt m&aacute;y ở m&aacute;i nh&agrave; (hoặc ngo&agrave;i trời) c&oacute; vị tr&iacute; tiếp nắng tốt v&agrave; kh&ocirc;ng bị che khuất. Tấm thu s&aacute;ng tốt nhất n&ecirc;n quay về hướng Nam với g&oacute;c nghi&ecirc;ng khoảng 15 độ.</p>\r\n\r\n<p>Nguồn cấp nước cho m&aacute;y phải ổn định v&igrave; m&aacute;y chỉ hoạt động khi được cấp nước đều đặn v&agrave; li&ecirc;n tục, nước kh&ocirc;ng nhiễm ph&egrave;n, nhiễm mặn.</p>\r\n\r\n<p>V&igrave; khi sử dụng, phải xả hết nước lạnh trong ống ra mới c&oacute; nước n&oacute;ng n&ecirc;n c&aacute;c gia đ&igrave;nh cần bố tr&iacute; đường ống c&agrave;ng ngắn c&agrave;ng tốt để tiết kiệm nước. Đường ống dẫn n&ecirc;n l&agrave; ống nước n&oacute;ng chuy&ecirc;n dụng.&nbsp;</p>\r\n\r\n<p>&quot;V&agrave; cuối c&ugrave;ng, bạn n&ecirc;n chọn nh&agrave; cung cấp uy t&iacute;n v&agrave; c&oacute; chế độ hậu m&atilde;i tốt&quot;, tiến sĩ Cường khuy&ecirc;n.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-11-16 04:17:50', '2019-11-16 04:17:50', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (19, 19, 1, 'upload/contents/19.jpeg', '7 thói quen gây lãng phí mỗi ngày nhiều người hay mắc', '7-thoi-quen-gay-lang-phi-moi-ngay-nhieu-nguoi-hay-mac', 'Vặn nước không chặt hay quên tắt đèn, bình nóng lạnh... sẽ gây tốn kém đáng kể.', '<p>H&atilde;y tr&aacute;nh những th&oacute;i quen dưới đ&acirc;y để bảo vệ cả v&iacute; tiền của bạn lẫn m&ocirc;i trường sống, theo&nbsp;<em>RD</em>:</p>\r\n\r\n<p>Mua thực phẩm t&ugrave;y hứng</p>\r\n\r\n<p>Thay v&igrave; mua đ&uacute;ng số lượng m&igrave;nh cần, bạn c&oacute; xu hướng mua theo hứng, sau đ&oacute; cho v&agrave;o tủ lạnh v&agrave; bỏ qu&ecirc;n. Khi bạn t&igrave;m thấy ch&uacute;ng, rau quả, thức ăn đ&atilde; kh&ocirc;ng c&ograve;n tươi, kh&ocirc;ng thể sử dụng. Giải ph&aacute;p cho việc n&agrave;y l&agrave; bạn n&ecirc;n ghi v&agrave;o giấy những thứ cần cho một tuần, sau đ&oacute; h&atilde;y đi si&ecirc;u thị. Tr&aacute;nh để trường hợp tủ lạnh chất đầy đồ nhưng bạn kh&ocirc;ng sử dụng nổi thứ n&agrave;o v&igrave; qu&aacute; hạn.</p>\r\n\r\n<p>Vặn v&ograve;i nước kh&ocirc;ng chặt</p>\r\n\r\n<p>Bạn n&ecirc;n ch&uacute; &yacute; tới h&oacute;a đơn nước: nếu tiền nước tăng đột biến, đ&atilde; đến l&uacute;c cần kiểm tra lại quy tr&igrave;nh sử dụng. Ngo&agrave;i yếu tố r&ograve; rỉ đường ống, bạn n&ecirc;n ch&uacute; &yacute; mỗi khi kh&oacute;a m&aacute;y, tắt van v&igrave; c&oacute; thể đ&oacute;ng chưa chặt khiến nước vẫn nhỏ giọt, g&acirc;y l&atilde;ng ph&iacute;.</p>\r\n\r\n<p>Bật đ&egrave;n tất cả c&aacute;c ph&ograve;ng v&agrave; qu&ecirc;n tắt</p>\r\n\r\n<p>N&ecirc;n duy tr&igrave; th&oacute;i quen tắt đ&egrave;n khi ra khỏi ph&ograve;ng để tiết kiệm điện. Bạn cũng cần tắt b&igrave;nh n&oacute;ng lạnh trước khi sử dụng, tắt TV trước khi đi ngủ... để giảm c&ocirc;ng suất ti&ecirc;u thụ điện cũng như hao m&ograve;n m&aacute;y m&oacute;c.</p>\r\n\r\n<p>Đ&oacute;ng cửa tủ lạnh kh&ocirc;ng chặt</p>\r\n\r\n<p>Theo thời gian, một số c&aacute;nh tủ lạnh kh&ocirc;ng c&ograve;n khả năng kh&iacute;t tốt do hỏng gioăng, khiến c&aacute;nh cửa bị hở. Điều n&agrave;y v&ocirc; t&igrave;nh l&agrave;m bạn tốn lượng điện đ&aacute;ng kể, chưa n&oacute;i đến việc tủ c&oacute; thể bị hỏng, thực phẩm b&ecirc;n trong cũng chịu ảnh hưởng.</p>\r\n\r\n<p>Sử dụng qu&aacute; nhiều t&uacute;i nilon</p>\r\n\r\n<p>N&ecirc;n hướng tới c&aacute;c loại t&uacute;i t&aacute;i sử dụng, th&acirc;n thiện hơn với m&ocirc;i trường. Thay v&igrave; mua c&aacute;c loại t&uacute;i x&eacute; để đựng v&agrave; vứt đi sau mỗi lần d&ugrave;ng, bạn c&oacute; thể mua hộp trữ thực phẩm.</p>\r\n\r\n<p>Chạy theo c&aacute;c đồ gia dụng mẫu mới</p>\r\n\r\n<p>C&aacute;c nh&atilde;n h&agrave;ng thường xuy&ecirc;n ra mắt sản phẩm mới với t&iacute;nh năng được quảng c&aacute;o rất hấp dẫn. Thực tế, nếu đồ cũ vẫn d&ugrave;ng tốt, bạn đừng ph&iacute; tiền mua đồ mới với những t&iacute;nh năng m&agrave; thậm ch&iacute; bạn kh&ocirc;ng bao giờ cần tới.&nbsp;</p>\r\n\r\n<p>Mua quần &aacute;o, gi&agrave;y d&eacute;p theo &quot;mốt&quot; khi tủ c&ograve;n đầy đồ</p>\r\n\r\n<p>Việc mua sắm v&ocirc; tội vạ nhưng kh&ocirc;ng để &yacute; trong tủ m&igrave;nh c&ograve;n bao nhi&ecirc;u đồ ch&iacute;nh l&agrave; th&oacute;i quen g&acirc;y phung ph&iacute; của nhiều chị em. N&ecirc;n kiểm tra tủ đồ của bạn sau mỗi m&ugrave;a, loại đi những chiếc kh&ocirc;ng th&iacute;ch hợp, quy&ecirc;n g&oacute;p cho những người cần đến ch&uacute;ng, sau đ&oacute;, bạn vạch ra kế hoạch mua sắm ph&ugrave; hợp.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-11-16 04:20:48', '2019-11-16 04:20:48', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (20, 20, 1, 'upload/contents/20.jpeg', '10 lưu ý để đồ điện trong nhà bền gấp nhiều lần', '10-luu-y-de-do-dien-trong-nha-ben-gap-nhieu-lan', 'Của bền tại người, vì thế muốn đồ trong nhà có tuổi thọ cao, bạn nên lưu ý một số điều cho từng loại đồ gia dụng', '<p>Tủ lạnh</p>\r\n\r\n<p>L&agrave; thiết bị chạy 24/7, tủ lạnh sau thời gian d&agrave;i sử dụng c&oacute; thể bị trục trặc, thường l&yacute; do đơn giản l&agrave;:&nbsp;Bụi bẩn t&iacute;ch tụ ở d&agrave;n ngưng tụ giải nhiệt gi&oacute; (c&oacute; thể gọi n&ocirc;m na l&agrave; cục n&oacute;ng, thường nằm ph&iacute;a sau của&nbsp;tủ lạnh). Khi bụi bẩn t&iacute;ch tụ v&agrave;o quạt, m&aacute;y sẽ kh&ocirc;ng thể tản nhiệt hiệu quả. Để xử l&yacute; vấn đề n&agrave;y, tốt nhất&nbsp;bạn n&ecirc;n xem hướng dẫn của nh&agrave; sản xuất để biết được thời gian định kỳ lau dọn d&agrave;n ngưng. Ngo&agrave;i ra,&nbsp;bạn cần k&ecirc; tủ c&aacute;ch tường &iacute;t nhất l&agrave; 3 cm để luồng kh&ocirc;ng kh&iacute; tho&aacute;t ra được lưu th&ocirc;ng. Cần lưu &yacute; để&nbsp;c&aacute;c v&ograve;ng đệm tr&ecirc;n thiết bị kh&ocirc;ng bị kh&ocirc;, nứt.</p>\r\n\r\n<p>M&aacute;y giặt</p>\r\n\r\n<p>Sau mỗi lần giặt, bạn n&ecirc;n mở cửa lồng giặt cho lồng kh&ocirc; r&aacute;o, tr&aacute;nh nấm mốc sinh s&ocirc;i. Nếu&nbsp;m&aacute;y của bạn rung, lắc, c&oacute; thể l&agrave; bạn đ&atilde; để lượng quần &aacute;o kh&ocirc;ng th&iacute;ch hợp, hoặc đặt ch&acirc;n m&aacute;y kh&ocirc;ng&nbsp;đ&uacute;ng c&aacute;ch.</p>\r\n\r\n<p>M&aacute;y rửa b&aacute;t</p>\r\n\r\n<p>C&aacute;c chất dầu mỡ c&oacute; trong thức ăn h&agrave;ng ng&agrave;y c&oacute; thể l&agrave;m tắc nghẽn bộ lọc, v&ograve;i trong m&aacute;y&nbsp;rửa. Thế n&ecirc;n, đừng đưa những b&aacute;t đũa b&aacute;m cặn nhiều dầu mỡ v&agrave;o m&aacute;y. N&ecirc;n mua c&aacute;c sản phẩm l&agrave;m sạch, v&iacute; dụ như vi&ecirc;n vệ sinh m&aacute;y rửa.</p>\r\n\r\n<p>L&ograve; nướng, l&ograve; vi s&oacute;ng</p>\r\n\r\n<p>N&ecirc;n đặt một tấm khay ở ngăn thấp nhất của l&ograve; nướng để hứng c&aacute;c thực phẩm tr&agrave;n. Nếu thức&nbsp;ăn bắn tung t&oacute;e, chỉ n&ecirc;n lau ch&ugrave;i l&ograve; sau khi n&oacute; đ&atilde; nguội hẳn. Một mẹo nhỏ tẩy rửa rất hiệu quả&nbsp;đ&atilde; được thử nghiệm: rắc v&agrave;o vết bẩn (c&ograve;n n&oacute;ng) một ch&uacute;t bột baking soda, để qua đ&ecirc;m, s&aacute;ng h&ocirc;m&nbsp;sau, bạn c&oacute; thể lau vết bẩn dễ d&agrave;ng.</p>\r\n\r\n<p>Với l&ograve; vi s&oacute;ng, n&ecirc;n lau ch&ugrave;i c&aacute;c vết bẩn ngay khi ch&uacute;ng xuất hiện, bởi khi b&aacute;m v&agrave;o m&aacute;y, ch&uacute;ng c&oacute; thể ăn&nbsp;m&ograve;n thiết bị, l&agrave;m giảm tuổi thọ. Mỗi tuần, bạn n&ecirc;n lau sạch cửa l&ograve;, c&aacute;c khung, bảng điều khiển bằng&nbsp;một miếng vải ẩm.</p>\r\n\r\n<p>M&aacute;y sấy quần &aacute;o</p>\r\n\r\n<p>Nếu quần &aacute;o mất qu&aacute; nhiều thời gian để kh&ocirc;, nhiều khả năng hệ thống&nbsp;th&ocirc;ng gi&oacute; đ&atilde; bị tắc nghẽn. N&ecirc;n loại bỏ những cặn b&atilde; như xơ vải... b&aacute;m v&agrave;o m&agrave;ng lọc bằng chổi l&ocirc;ng,&nbsp;hoặc thay thế bộ lọc nếu thấy vết r&aacute;ch hay lỗ thủng. Mỗi năm một lần, bạn th&aacute;o ống th&ocirc;ng hơi bằng&nbsp;nh&ocirc;m ra khỏi m&aacute;y sấy v&agrave; l&agrave;m sạch xơ vải bằng b&agrave;n chải ph&ugrave; hợp.</p>\r\n\r\n<p>M&aacute;y pha c&agrave; ph&ecirc;</p>\r\n\r\n<p>Thay bộ lọc hai th&aacute;ng một lần, hoặc theo hướng dẫn của nh&agrave; sản xuất.</p>\r\n\r\n<p>M&aacute;y h&uacute;t bụi</p>\r\n\r\n<p>Lau dọn m&agrave;ng lọc của m&aacute;y h&uacute;t bụi mỗi th&aacute;ng bằng nước, sau đ&oacute; phơi kh&ocirc;.</p>\r\n\r\n<p>M&aacute;y h&uacute;t m&ugrave;i</p>\r\n\r\n<p>Bộ lọc dầu mỡ bằng nh&ocirc;m được l&agrave;m sạch ba th&aacute;ng một lần với nước ấm, x&agrave; ph&ograve;ng.</p>\r\n\r\n<p>Điều h&ograve;a</p>\r\n\r\n<p>Vệ sinh bộ lọc sau mỗi m&ugrave;a.</p>\r\n\r\n<p>Quạt th&ocirc;ng gi&oacute; trong ph&ograve;ng tắm</p>\r\n\r\n<p>L&agrave;m sạch quạt hai lần mỗi năm. Nếu th&aacute;o được nắp quạt, bạn c&oacute; thể&nbsp;lau c&aacute;c c&aacute;nh quạt bằng vải mềm, kết hợp dung dịch đa năng.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-11-16 04:23:22', '2019-11-16 04:23:24', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_categories
-- ----------------------------
DROP TABLE IF EXISTS `menu_categories`;
CREATE TABLE `menu_categories`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `is_banner` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`menu_id`, `category_id`) USING BTREE,
  INDEX `menu_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `menu_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_categories_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_categories
-- ----------------------------
INSERT INTO `menu_categories` VALUES (1, 5, '2019-10-09 08:54:46', '2019-10-09 08:54:46', NULL, 0);
INSERT INTO `menu_categories` VALUES (9, 5, '2019-09-25 07:28:10', '2019-09-25 07:28:10', NULL, 0);

-- ----------------------------
-- Table structure for menu_contents
-- ----------------------------
DROP TABLE IF EXISTS `menu_contents`;
CREATE TABLE `menu_contents`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `content_id`) USING BTREE,
  INDEX `menu_contents_content_id_foreign`(`content_id`) USING BTREE,
  CONSTRAINT `menu_contents_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_contents_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_contents
-- ----------------------------
INSERT INTO `menu_contents` VALUES (2, 1, '2019-09-25 07:11:10', '2019-09-25 07:11:10', NULL);

-- ----------------------------
-- Table structure for menu_products
-- ----------------------------
DROP TABLE IF EXISTS `menu_products`;
CREATE TABLE `menu_products`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `product_id`) USING BTREE,
  INDEX `menu_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `menu_products_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_types
-- ----------------------------
DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE `menu_types`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `type_id`) USING BTREE,
  INDEX `menu_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `menu_types_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_types
-- ----------------------------
INSERT INTO `menu_types` VALUES (3, 5, '2019-11-14 14:27:30', '2019-11-14 14:27:30', NULL);
INSERT INTO `menu_types` VALUES (5, 3, '2019-11-14 14:27:45', '2019-11-14 14:27:45', NULL);
INSERT INTO `menu_types` VALUES (6, 2, '2019-11-14 14:27:58', '2019-11-14 14:27:58', NULL);
INSERT INTO `menu_types` VALUES (7, 1, '2019-11-15 13:22:46', '2019-11-15 13:22:46', NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `banner` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 1, 0, 'Trang Chủ', '/', 'home', '', 1, 1, 'vi', NULL, '2019-09-25 03:46:51', '2019-09-25 04:09:27', NULL);
INSERT INTO `menus` VALUES (2, 2, 0, 'Giới Thiệu', 'gioi-thieu', 'about', '', 1, 2, 'vi', NULL, '2019-09-25 04:26:43', '2019-09-25 07:11:10', NULL);
INSERT INTO `menus` VALUES (3, 3, 4, 'Máy Nước Nóng', 'may-nuoc-nong', 'type', '', 1, 1, 'vi', NULL, '2019-09-25 07:08:34', '2019-11-14 14:27:17', NULL);
INSERT INTO `menus` VALUES (4, 4, 0, 'Sản Phẩm', 'san-pham', 'null', '', 1, 3, 'vi', NULL, '2019-09-25 07:16:58', '2019-09-25 07:22:20', NULL);
INSERT INTO `menus` VALUES (5, 5, 4, 'Bồn Inox', 'bon-inox', 'type', '', 1, 2, 'vi', NULL, '2019-09-25 07:24:33', '2019-11-14 14:27:45', NULL);
INSERT INTO `menus` VALUES (6, 6, 4, 'Bồn Nhựa', 'bon-nhua', 'type', '', 1, 3, 'vi', NULL, '2019-09-25 07:25:23', '2019-11-14 14:27:57', NULL);
INSERT INTO `menus` VALUES (7, 7, 4, 'Chậu Rửa', 'chau-rua', 'type', '', 1, 4, 'vi', NULL, '2019-09-25 07:25:53', '2019-11-15 13:22:46', NULL);
INSERT INTO `menus` VALUES (9, 9, 0, 'Tin Tức', 'tin-tuc', 'category', NULL, 1, 5, 'vi', NULL, '2019-09-25 07:28:08', '2019-09-25 07:28:08', NULL);
INSERT INTO `menus` VALUES (11, 11, 0, 'Liên Hệ', 'lien-he', 'contact', '', 1, 7, 'vi', NULL, '2019-09-25 07:29:39', '2019-10-08 10:01:59', NULL);
INSERT INTO `menus` VALUES (12, 12, 0, 'Giỏ Hàng', 'gio-hang', 'cart', '', 0, 0, 'vi', NULL, '2019-10-04 07:42:27', '2019-10-08 10:01:01', NULL);
INSERT INTO `menus` VALUES (16, 13, 0, 'Thanh Toán', 'thanh-toan', 'order', '', 0, 0, 'vi', NULL, '2019-10-09 02:24:05', '2019-10-09 02:24:19', NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_06_07_092547_create_types_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_06_07_092557_create_products_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_06_07_092743_create_product_types_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_07_15_171057_create_languages_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_07_15_172233_create_menus_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_07_15_173740_create_contents_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_07_16_101957_create_categories_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_07_16_102230_create_menu_categories_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_07_16_102328_create_menu_contents_table', 1);
INSERT INTO `migrations` VALUES (12, '2019_07_16_102429_create_content_categories_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_07_16_151845_allow_null_target_menus_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_07_19_161724_create_menu_products_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_07_19_161841_create_menu_types_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_08_14_104127_create_sponsors_table', 2);
INSERT INTO `migrations` VALUES (17, '2019_08_21_143215_add_url_target_sort_is_show_to_sponsors_table', 3);
INSERT INTO `migrations` VALUES (19, '2019_09_25_085431_create_carousels_table', 4);
INSERT INTO `migrations` VALUES (21, '2019_09_26_071649_create_contacts_table', 5);
INSERT INTO `migrations` VALUES (23, '2019_09_26_082049_add_video_to_contents_table', 6);
INSERT INTO `migrations` VALUES (27, '2019_09_26_092326_add_is_recruit_to_categories_table', 7);
INSERT INTO `migrations` VALUES (28, '2019_07_23_103255_add_is_featured_to_products_table', 8);
INSERT INTO `migrations` VALUES (30, '2019_10_04_094036_create_sessions_table', 9);
INSERT INTO `migrations` VALUES (35, '2019_10_07_000000_add_alias_to_products_table', 10);
INSERT INTO `migrations` VALUES (36, '2019_10_07_033910_add_description_to_products_table', 10);
INSERT INTO `migrations` VALUES (37, '2019_10_08_094906_add_banner_to_menus_table', 11);
INSERT INTO `migrations` VALUES (38, '2019_10_09_020143_create_payments_table', 12);
INSERT INTO `migrations` VALUES (42, '2019_10_09_082400_add_banner_to_menu_categories_table', 13);
INSERT INTO `migrations` VALUES (43, '2019_10_11_031638_create_orders_table', 14);
INSERT INTO `migrations` VALUES (44, '2019_10_11_032519_create_order_products_table', 15);
INSERT INTO `migrations` VALUES (46, '2019_10_12_083128_add_foreign_key_to_order_products_table', 16);
INSERT INTO `migrations` VALUES (49, '2019_10_12_094225_add_total_to_orders_table', 17);
INSERT INTO `migrations` VALUES (50, '2019_10_29_070521_add_bill_price_to_order_products_table', 18);

-- ----------------------------
-- Table structure for order_products
-- ----------------------------
DROP TABLE IF EXISTS `order_products`;
CREATE TABLE `order_products`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `bill_price` int(10) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`product_id`, `order_id`) USING BTREE,
  INDEX `order_products_order_id_foreign`(`order_id`) USING BTREE,
  CONSTRAINT `order_products_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `order_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Họ',
  `last_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Tên',
  `phone` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Điện thoại',
  `address` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Địa chỉ',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Email',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `sum` int(10) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for payments
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT 'Người tạo',
  `content` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Nội dung',
  `total` decimal(10, 2) NOT NULL DEFAULT 0.00 COMMENT 'Tổng cộng',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `payments_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `payments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product_types
-- ----------------------------
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE `product_types`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`, `type_id`) USING BTREE,
  INDEX `product_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `product_types_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `product_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_types
-- ----------------------------
INSERT INTO `product_types` VALUES (1, 5, NULL, NULL);
INSERT INTO `product_types` VALUES (2, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (3, 1, NULL, NULL);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Alias',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Mô tả',
  `receipt_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá nhập',
  `bill_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá bán',
  `stock` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Tồn kho',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `is_featured` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Sản phẩm nổi bật',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'upload/products/1.jpeg', 'Máy nước nóng năng lượng mặt trời ĐT 130L 58-12 CLASSIC', 'may-nuoc-nong-nang-luong-mat-troi-dt-130l-58-12-classic', 'Được sản xuất trên dây chuyền theo tiêu chuẩn Châu Âu.\r\nCông nghệ phun sơn tĩnh điện chịu được các khí hậu vùng biển.\r\nLớp bảo ôn: Ruột bình bảo ôn được làm bằng chất liệu inox SUS 304, độ bền cao với độ dày 0.4mm, đảm bảo an toàn vệ sinh thực phẩm. Lớp giữ nhiệt bằng hợp chất foam (độ dày 55mm) độ nén chặt và giữ nhiệt độ lên đến 96 giờ.\r\nVỏ bình bảo ôn: được làm từ nguyên liệu Inox SUS 304/BA chịu được các kiểu thời tiết khắc nghiệt.\r\nChân máy: nguyên liệu Inox siêu bền, đảm bảo độ cứng, vững và chịu lực tốt.', 0, 6900000, 0, 1, 0, 'Được sản xuất trên dây chuyền theo tiêu chuẩn Châu Âu.\r\nCông nghệ phun sơn tĩnh điện chịu được các khí hậu vùng biển.\r\nLớp bảo ôn: Ruột bình bảo ôn được làm bằng chất liệu inox SUS 304, độ bền', '2019-11-15 07:37:03', '2019-11-15 07:37:04', NULL);
INSERT INTO `products` VALUES (2, 'upload/products/2.png', 'Bồn nước inox I1.000 Ngang - SUS 304', 'bon-nuoc-inox-i1000-ngang-sus-304', 'Vật liệu : SUS 304 siêu bền\r\nCông nghệ hàn lăn tự động\r\nLogo dập nổi chống hàng giả\r\nVật liệu : SUS 304 siêu bền\r\nCông nghệ hàn lăn tự động\r\nLogo dập nổi chống hàng giả', 0, 3770000, 0, 1, 0, 'Vật liệu : SUS 304 siêu bền\r\nCông nghệ hàn lăn tự động\r\nLogo dập nổi chống hàng giả', '2019-11-15 07:49:03', '2019-11-15 07:49:03', NULL);
INSERT INTO `products` VALUES (3, 'upload/products/3.jpeg', 'CHẬU RỬA CAO CẤP DX42001', 'chau-rua-cao-cap-dx42001', 'Xuất xứ  :Chính hãng\r\nChất liệu: Inox SUS 304\r\nCông nghệ dập liền nguyên khối\r\nVượt trội về chiều sâu', 0, 2850000, 0, 1, 0, 'Xuất xứ  :Chính hãng\r\nChất liệu: Inox SUS 304\r\nCông nghệ dập liền nguyên khối\r\nVượt trội về chiều sâu', '2019-11-15 07:56:09', '2019-11-15 07:56:10', NULL);

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE INDEX `sessions_id_unique`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sponsors
-- ----------------------------
DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE `sponsors`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `is_show` tinyint(1) NULL DEFAULT 1,
  `sort` int(10) UNSIGNED NULL DEFAULT 0,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `sponsor_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sponsors
-- ----------------------------
INSERT INTO `sponsors` VALUES (4, 'upload/sponsors/4.png', 1, 0, NULL, 'www.solartek.com.vn/', 'Solar Tek', NULL, '2019-09-25 08:42:28', '2019-11-14 14:52:36');
INSERT INTO `sponsors` VALUES (5, 'upload/sponsors/5.png', 1, 0, NULL, 'https://www.megasun.com.vn/v2/', 'MEGASUN', NULL, '2019-09-25 08:43:27', '2019-11-14 14:53:08');
INSERT INTO `sponsors` VALUES (6, 'upload/sponsors/6.png', 1, 0, NULL, '/', 'Solar Energy', NULL, '2019-11-14 14:55:34', '2019-11-14 14:55:34');
INSERT INTO `sponsors` VALUES (7, 'upload/sponsors/7.png', 1, 0, NULL, 'http://www.redsun.com.vn/', 'Mặt Trời Đỏ', NULL, '2019-11-14 14:56:23', '2019-11-14 14:56:24');

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `type_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT 'Sắp xếp',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES (1, 'upload/types/1.jpeg', 'Chậu Rửa', 1, 1, NULL, '2019-09-25 07:15:36', '2019-11-15 07:54:58', NULL);
INSERT INTO `types` VALUES (2, 'upload/types/2.jpeg', 'Bồn Nhựa', 1, 2, NULL, '2019-09-25 07:23:08', '2019-11-15 03:37:18', NULL);
INSERT INTO `types` VALUES (3, 'upload/types/3.jpeg', 'Bồn Inox', 1, 3, NULL, '2019-09-25 07:23:27', '2019-11-15 03:37:11', NULL);
INSERT INTO `types` VALUES (5, 'upload/types/5.jpeg', 'Máy Nước Nóng', 1, 4, NULL, '2019-09-25 07:23:38', '2019-11-15 03:35:16', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'InnoSoft', 'innosoft', 'innosoftcantho@gmail.com', NULL, '$2y$10$HTPZows7CAP2t76GLm9B5ONH.0ARunk8Zom9kY59qmtTE0rjDYK8G', 1, '28UeSv44RPW7kjfIcVfDTHUZLrJHpAjfLdPW0EQ6N739bMlIayxjMGaB4Ttk', '2019-09-25 03:45:55', '2019-11-13 04:21:59', NULL);

SET FOREIGN_KEY_CHECKS = 1;
